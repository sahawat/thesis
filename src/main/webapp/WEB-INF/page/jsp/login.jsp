
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" >
	  
<head>
	<meta charset="UTF-8"/>
	<title>Login Page</title>
</head>
<body>
	<div class="starter-template">
		<form class="form-inline" role="form"  method="post">
			<div>
				<label for="username">Username : </label>
				<input type="text" id="username" name="username"/>
			</div>
			<div>
				<label for="password">Password : </label>
				<input type="password" id="password" name="password"/>
			</div>
			<div>
				<input type="submit" value="Sign In"/>
			</div>
		</form>
	</div>
</body>
</html>