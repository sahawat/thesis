package com.cu.thesis.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class HelloWorldController {
	
	private static Logger LOG = LoggerFactory.getLogger(HelloWorldController.class);
	
	@Value(value="${application.message}")
	private String message;
	
	@RequestMapping(value="/",method = RequestMethod.GET)
	public String init(Model model) {
		System.out.println("**************");
		System.out.println("hello : "+ message);

		model.addAttribute("message",message);
		return "index";
	}
	

}
